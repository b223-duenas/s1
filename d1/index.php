<!-- 
	Serving PHP files:
	php -S localhost:8000
 -->

 <!-- 
	- "code.php" is used for defining php statements and functions.
	- "index.php" for embedding php in HTML to be serve and shown in our browser.

  -->
<!-- PHP code can be included to another file by using the require keyword. -->
<!-- We will use this method to separate the declaration of variables and functions from the HTML content. -->
<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S01: PHP Basics and Selection Control</title>
	</head>
	<body>
		<!-- <h1>Hello World!</h1> -->

		<h1>Echoing Values</h1>
		<!-- Single quoute cannot output the variable values. -->
		<p><?php echo 'Good day $name! Your given email is $email.'; ?></p>

		<!-- Single quote can be use but concatenation is needed (.) is concatenate operator -->
		<p><?php echo 'Good day ' . $name . '! ' . 'Your given email is ' . $email . '.'; ?></p>

		<!-- Double quote can easily read the variable -->
		<p><?php echo "Good day $name! Your given email is $email."; ?></p>

		<p><?php echo 'The value of Pi: ' . PI; ?></p>

		<!-- Normal echoing of boolean and null variables will not make it visible to the web page. -->
		<!--
			- The var_dump() function dumps information about one or more variables.
			- It displays structured information about variables/expressions including its type and value.
		-->
		<!-- gettype() retuns the type of a variable -->
		<h1>Data Types</h1>
		<p><?php echo $hasTravelledAbroad; ?></p>
		<p><?php echo $spouse; ?></p>

		<!-- To see their types instead, we can use var_dump() function -->
		<p><?php echo gettype($hasTravelledAbroad); ?></p>
		<p><?php echo gettype($spouse); ?></p>

		<p><?php echo var_dump($hasTravelledAbroad); ?></p>
		<p><?php echo var_dump($spouse); ?></p>

		<p><?php var_dump($gradesObj); ?></p>
		<p><?php echo $gradesObj->firstGrading; ?></p>
		<p><?php echo $personObj->address->state; ?></p>

		<p><?php echo $grades[3]; ?></p>
		<p><?php echo $grades[2]; ?></p>

		<h1>Operators</h1>
		<p>X: <?php echo $x; ?></p>
		<p>Y: <?php echo $y; ?></p>

		<p>is Legal Age: <?php var_dump($isLegalAge); ?></p>
		<p>is Registered: <?php var_dump($isRegistered); ?></p>

		<h2>Arithmetic Operators</h2>
		<p>Sum: <?php echo $x + $y; ?></p>
		<p>Difference: <?php echo $x - $y; ?></p>
		<p>Product: <?php echo $x * $y; ?></p>
		<p>Quotient: <?php echo $x / $y; ?></p>
		<p>Modulo: <?php echo $x % $y; ?></p>

		<h2>Equality Operator</h2>
		<p>Loose Equality: <?php var_dump($x == '1342.14') ?></p>
		<p>Strict Equality: <?php var_dump($x === '1342.14') ?></p>
		<!-- Strict equality/inequality is preferred so that we can check for both of the value and data type two given values. -->
		<p>Loose Inequality: <?php var_dump($x != '1342.14') ?></p>
		<p>Strict Inequality: <?php var_dump($x !== '1342.14') ?></p>

		<h2>Greater/Lesser Operators</h2>
		<p>Is Lesser: <?php var_dump($x < $y); ?></p>
		<p>Is Greater: <?php var_dump($x > $y); ?></p>
		<p>Is Lesser or equal: <?php var_dump($x <= $y); ?></p>
		<p>Is Greater or equal: <?php var_dump($x >= $y); ?></p>

		<h2>Logical Operators</h2>
		<!-- Logical operators are used to verify whether an expression or group of expressions are either true or false. -->
		<p>Are All Requirements Met: <?php var_dump($isLegalAge && $isRegistered); ?></p>
		<p>Are Some Requirements Met: <?php var_dump($isLegalAge || $isRegistered); ?></p>
		<p>Are Some Requirements Not Met: <?php var_dump($isLegalAge && !$isRegistered); ?></p>

		<h1>Function</h1>
		<p>Full Name: <?php echo getFullName('John', 'D', 'Smith'); ?></p>

		<h1>Selection Control Structures</h1>
		<h2>If-ElseIf-Else</h2>
		<p><?php echo determineTyphoonIntensity(12); ?></p>

		<h2>Ternary Sample</h2>
		<p>78: <?php echo isUnderAge(78); ?></p>
		<p>17: <?php echo isUnderAge(17); ?></p>

		<h2>Switch Case</h2>
		<p><?php echo determineComputerUser(5); ?></p>
		<p><?php echo determineComputerUser(10); ?></p>

		<h2>Try-Catch-Finally</h2>
		<p><?php greeting("Hello"); ?></p>
		<p><?php greeting(25); ?></p>

	</body>
</html>