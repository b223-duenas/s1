<?php

// [SECTION] Comments
//  ctrl + / (single line comment)
// ctrl + shift + / (multiline comment)

// [SECTION] Variables
// Variables are defined using dollar ($) notation before the name of the variable.
$name = 'John Smith';
$email = 'johnsmith@mail.com';

// [SECTION] Constants
// Constants are defined using the "define()" function.
// Naming convention for "constants" should be in ALL CAPS.
// Doesn't use the $ notation before the variable.

define('PI', 3.1416);

// [SECTION] Data Types

// Strings
$state = 'New York';
$country = 'United States of America';
$address = $state .', '.$country; // concatenation via dot (.) operator
$address = "$state, $country"; // concatenation using double quotes.

// Integer
$age = 31;
$headcount = 26;

// Floats
$grade = 98.2;
$distanceInKilometers = 1342.12;

// Boolean
$hasTravelledAbroad = false;
$haveSymptoms = true;

// Arrays
$grades = array(98.7, 92.1, 90.2, 94.6);

// Null
$spouse = null;
$middleName = null;

// Objects
$gradesObj = (object)[
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6
];

$personObj = (object)[
	'fullName' => 'John Smith',
	'isMarried' => false,
	'age' => 35,
	'address' => (object)[
		'state' => 'New York',
		'country' => 'United States of America'
	]
];

// [SECTION] Operators

// Assignment Operators

$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;

// [SECTION] Functions
function getFullName($firstName, $middleInitial, $lastName){
	return "$lastName, $firstName $middleInitial";
}

// If-ElseIf-Else Statement

function determineTyphoonIntensity($windSpeed){
	if($windSpeed <30 ){
		return 'Not a typhoon yet';
	}
	else if ($windSpeed <= 61){
		return 'Tropical Depression detected.';
	}
	else if($windSpeed >= 62 && $windSpeed <= 88){
		return 'Tropical Storm detected.';
	}
	else if($windSpeed >= 89 && $windSpeed <= 117){
		return 'Severe Tropical Storm detected.';
	}
	else{
		return 'Typhoon detected.';
	}
}

// Conditional (Ternary) Operator
function isUnderAge($age){
	return ($age < 18) ? 'under age' : 'legal age';
}

// Switch Statement
function determineComputerUser($computerNumber){
	switch ($computerNumber) {
		case 1:
			return 'Linus Torvalds';
			break;
		case 2:
			return 'Steve Jobs';
			break;
		case 3:
			return 'Sid Meier';
			break;
		case 4:
			return 'Onel De Guzman';
			break;
		case 5:
			return 'Christian Salvador';
			break;
		default:
			return $computerNumber . ' is out of bounds.';
			break;
	}
}

// Try-Catch-Finally Statement

function greeting($str){
	try{
		 // Attempt to execute a code.
		if(gettype($str) === "string"){
			echo $str;
		}
		else{
			//instantiate a new Exception object from PHP's pre-defined Exception class using its constructor
	            //the Exception object created here will have a message "Oops!"
			throw new Exception("Oops!");
		}
	}
	catch (Exception $e){
		// Catch errors within 'try', in this case the error is not a "string" data type.
	        // $e is an object we a getter function "getMessage()"
		echo $e->getMessage();
	}
	finally{
		// Continue execution of code regardless of success and failure of code execution in 'try' block.
		echo " I did it again!";
	}
}